# NodeJS GSM API

A simple and easy to use NodeJS API to communicate with serial GSM Modems.

Original author: [stasel](https://github.com/stasel/NodeGSM)

**Note**: Project has been rewritten in TypeScript by [mtyszczak](https://gitlab.com/mtyszczak) and is designed to be work with SIM7000 Series GSM Modems

## Features
* Read device information: serial, manufacturer, model.
* Read network information: Carrier, signal strength, subscriber id.
* Read and send SMS Messages.
* Add, Read and modify SIM card contacts.
* Make calls (without audio).

## Requirements
* Connected USB GSM Modem
* Working SIM Card
* NodeJS 11 and later

## Build
In order to build this project you have to execute given commands:
```bash
# Install required binaries (Node.js and Native Package Manager)
sudo apt install -y \
      nodejs \
      npm

# Install TypeScript compiler globally
## You can also install it localy if you want to
sudo npm install -g typescript # ts-node # if you are a developer

# Install project dependencies
## (you have to be in a project root directory)
npm install

# Compile project using TypeScript compiler
## (output will be located in the `data` directory)
npm run build
```

### Cleanup
If you want to clear your current working directory from the untracked, bot data-related directories you can run:
```bash
npm run clear
```
which removes `dist` directories along with their content

## Usage
```ts
import GSM from "nodegsm";

const gsm = new GSM( "/dev/gsmmodem" );

await gsm.connect()

console.info( "Phone signal:", await gsm.getSignalQuality() );
console.info( "Current operator:", await gsm.getCurrentOperator() );

gsm.disconnect();
```

## Resources
* [
Send and Receive SMS Messages Using Raspberry Pi and Python
](https://hristoborisov.com/index.php/projects/turning-the-raspberry-pi-into-a-sms-center-using-python/)
* [AT Commands Reference Guide  ](https://www.sparkfun.com/datasheets/Cellular%20Modules/AT_Commands_Reference_Guide_r0.pdf) ([Local Copy](docs/AT_Commands_Reference_Guide_r0.pdf))
* [
Introduction to AT commands and its uses
](https://www.codeproject.com/Articles/85636/Introduction-to-AT-commands-and-its-uses)

## LICENSE
MIT License

See [LICENSE.md](LICENSE.md)

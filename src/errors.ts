/**
 * @fileoverview GSM Errors
 * @note Rewritten in typescript by mtyszczak
 * @author Stasel
 * @license MIT
 */

export class TimeoutError extends Error {
  public name = "TimeoutError";

  public constructor( msg:string ) { super(msg); }
};

export class ConnectionError extends Error {
  public name = "ConnectionError";

  public constructor( msg:string ) { super(msg); }
}

export class ResponseError extends Error {
  public name = "ResponseError";

  public constructor( msg:string ) { super(msg); }
};

export class ParseError extends Error {
  public name = "ParseError";

  public constructor( msg:string ) { super(msg); }
};

export class UnsupportedEventName extends Error {
  public name = "UnsupportedEventName";

  public constructor( msg:string ) { super(msg); }
};

/**
 * ME Error Result Codes
 */
export enum MEError {
  "phone failure"                                    = 0,
  "No connection to phone"                           = 1,
  "phone-adaptor link reserved"                      = 2,
  "operation not allowed"                            = 3,
  "operation not supported"                          = 4,
  "PH-SIM PIN required"                              = 5,
  "SIM not inserted"                                 = 10,
  "SIM PIN required"                                 = 11,
  "SIM PUK required"                                 = 12,
  "SIM failure"                                      = 13,
  "SIM busy"                                         = 14,
  "SIM wrong"                                        = 15,
  "incorrect password"                               = 16,
  "SIM PIN2 required"                                = 17,
  "SIM PUK2 required"                                = 18,
  "memory full"                                      = 20,
  "invalid index"                                    = 21,
  "not found"                                        = 22,
  "memory failure"                                   = 23,
  "text string too long"                             = 24,
  "invalid characters in text string"                = 25,
  "dial string too long"                             = 26,
  "invalid characters in dial string"                = 27,
  "no network service"                               = 30,
  "network timeout"                                  = 31,
  "network not allowed - emergency calls only"       = 32,
  "network personalization PIN required"             = 40,
  "network personalization PUK required"             = 41,
  "network subset personalization PIN required"      = 42,
  "network subset personalization PUK required"      = 43,
  "service provider personalization PIN required"    = 44,
  "service provider personalization PUK required"    = 45,
  "corporate personalization PIN required"           = 46,
  "corporate personalization PUK require"            = 47,

  // Easy CAMERA® related errors
  "Camera not found"                                 = 50,
  "Camera Initialization Error"                      = 51,
  "Camera not Supported"                             = 52,
  "No Photo Taken"                                   = 53,
  "NET BUSY...Camera TimeOut"                        = 54,
  "Camera Error"                                     = 55,

  // General purpose error
  "Unknown error"                                    = 100,

  // GPRS related errors to a failure to perform an Attach:
  "Illegal MS (#3)*"                                 = 103,
  "Illegal ME (#6)*"                                 = 106,
  "GPRS service not allowed (#7)*"                   = 107,
  "PLMN not allowed (#11)*"                          = 111,
  "Location area not allowed (#12)*"                 = 112,
  "Roaming not allowed in this location area (#13)*" = 113,

  // GPRS related errors to a failure to Activate a Context and others:
  "service option not supported (#32)*"              = 132,
  "requested service option not subscribed (#33)*"   = 133,
  "service option temporarily out of order (#34)*"   = 134,
  "unspecified GPRS error"                           = 148,
  "PDP authentication failure"                       = 149,
  "invalid mobile class"                             = 150,

  // Network survey errors:
  "Network survey error (No Carrier)*"               = 257,
  "Network survey error (Busy)*"                     = 258,
  "Network survey error (Wrong request)*"            = 259,
  "Network survey error (Aborted)* "                 = 260,

  // Easy GPRS® related errors:
  "generic undocumented error"                       = 400,
  "GPRS wrong state"                                 = 401,
  "wrong mode"                                       = 402,
  "context already activated"                        = 403,
  "stack already active"                             = 404,
  "activation failed"                                = 405,
  "context not opened"                               = 406,
  "cannot setup socket"                              = 407,
  "cannot resolve DN"                                = 408,
  "timeout in opening socket"                        = 409,
  "cannot open socket"                               = 410,
  "remote disconnected or timeout"                   = 411,
  "connection failed"                                = 412,
  "tx error"                                         = 413,
  "already listening"                                = 414,

  // FTP related errors:
  "ok"                                               = 420,
  "connect"                                          = 421,
  "disconnect"                                       = 422,
  "error"                                            = 423,
  "FTP wrong state"                                  = 424,
  "can not activate"                                 = 425,
  "can not resolve name"                             = 426,
  "can not allocate control socket"                  = 427,
  "can not connect control socket"                   = 428,
  "bad or no response from server"                   = 429,
  "not connected"                                    = 430,
  "already connected"                                = 431,
  "context down"                                     = 432,
  "no photo available"                               = 433,
  "can not send photo"                               = 434
}

/**
 * Message Service Failure Result Codes
 */
export enum MSError {
  "ME failure"                        = 300,
  "SMS service of ME reserved"        = 301,
  "operation not allowed"             = 302,
  "operation not supported"           = 303,
  "invalid PDU mode parameter"        = 304,
  "invalid text mode parameter"       = 305,
  "SIM not inserted"                  = 310,
  "SIM PIN required"                  = 311,
  "PH-SIM PIN required"               = 312,
  "SIM failure"                       = 313,
  "SIM busy"                          = 314,
  "SIM wrong"                         = 315,
  "SIM PUK required"                  = 316,
  "SIM PIN2 required"                 = 317,
  "SIM PUK2 required"                 = 318,
  "memory failure"                    = 320,
  "invalid memory index"              = 321,
  "memory full"                       = 322,
  "SMSC address unknown"              = 330,
  "no network service"                = 331,
  "network timeout"                   = 332,
  "no +CNMA acknowledgement expected" = 340,
  "Unknown error"                     = 500,
}

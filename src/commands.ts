/**
 * @fileoverview GSM modem commands
 * @author mtyszczak
 * @license MIT
 */

import { CommandPrefixType } from './constants';
import { ParseError } from './errors';

/**
 * All of the built-in supported commands (You can also build your own using the {@link #make} function)
 */
export enum Commands {
  check                        = "",
  checkSignalQuality           = "+CSQ",
  checkCurrentOperator         = "+COPS",
  checkManufacturerInformation = "+CGMI",
  checkModelIdentification     = "+CGMM",
  checkRevisionIdentification  = "+CGMR",
  checkSerialNumber            = "+CGSN",
  checkSubscriberId            = "+CIMI",
  identificationInformation    = "I",
  dial                         = "D",
  answer                       = "A",
  autoAnswerRings              = "S0",
  hangup                       = "+CHUP",
  characterSet                 = "+CSCS",
  phoneBookStorage             = "+CPBS",
  phoneBookRead                = "+CPBR",
  phoneBookWrite               = "+CPBW",
  messageFormat                = "+CMGF",
  textModeParameters           = "+CSDH",
  preferredMessageStorage      = "+CPMS",
  listPreferredStoreMessages   = "+CMGL",
  sendMessage                  = "+CMGS",
  deleteMessage                = "+CMGD"
}

export enum UnsolicitedResultCodes {
  messageReceived              = "+CMTI"
}

export type CommandRequest =
  `${(
     `${CommandPrefixType}`
   | `${CommandPrefixType}${Commands}`
   | `${CommandPrefixType}${Commands}?`
   | `${CommandPrefixType}${Commands}=${string}`
   | `${string}`
  )}\r`;

/**
 * Parsed response as arguments
 */
export type CommandResponseArguments = Array<string>;

export type CommandResponse = `${CommandRequest}\r\n${string}\r\n`;

export type MakeSettings = {
  prefix?:string;
  commandArgsDelim?:string;
  command?:string;
  suffix?:string;
  args?:Array<unknown>;
};

const defaultMakeSettings:MakeSettings = {
  prefix: "AT",
  commandArgsDelim: "",
  suffix: "",
  args: []
};

/**
 * Converts MakeSettings object to the proper GSM module request command string
 * @param {MakeSettings} settings settings to be converted
 * @returns {string} request compatible with {@link CommandRequest}
 */
export const makeSettingsSyntaxToRawRequest
 = ( settings:MakeSettings ) => `${settings.prefix}${settings.command}${settings.commandArgsDelim}${settings?.args?.join(",") || ""}${settings.suffix}\r`;

/**
 * Create command request
 * @param {Commands|string} command command from which the CommandRequest will be created
 * @returns {CommandRequest} command request ready to be ran
 */
export const make = ( command:Commands|string, settings:MakeSettings = {} ):CommandRequest => {
  settings = { ...defaultMakeSettings, ...settings };
  if( typeof settings.command === "undefined" )
    settings.command = command;

  settings.args = ( settings.args as Array<unknown> ).map( (arg:unknown) => {
    switch( typeof arg ) {
    case "boolean":
      return arg ? 1 : 0;
    case "number":
      return Math.round( arg );
    case "string":
      return `"${arg}"`;
    default:
      throw new ParseError( `Type not supported in make command: ${typeof arg}` );
    }
  });

  return `${settings.prefix}${settings.command}${settings.commandArgsDelim}${settings.args.join(",") || ""}${settings.suffix}\r`;
};


/**
 * Create test command request
 * @param {Commands|string} command command from which the CommandRequest will be created
 * @returns {CommandRequest} command request ready to be ran
 */
export const makeTest = ( command:Commands|string ):CommandRequest => make( command, { commandArgsDelim: "=?" } );


/**
 * Read command
 * @param {Commands|string} command command from which the CommandRequest will be created
 * @returns {CommandRequest} command request ready to be ran
 */
export const makeRead = ( command:Commands|string ):CommandRequest => make( command, { commandArgsDelim: "?" } );


/**
 * Create test command request
 * @param {Commands|string} command command from which the CommandRequest will be created
 * @param {Array<unknown>} args array of arguments to write
 * @returns {CommandRequest} command request ready to be ran
 */
export const makeWrite = ( command:Commands|string, ...args:Array<unknown> ):CommandRequest => make( command, { commandArgsDelim: "=", args } );

/**
 * @fileoverview GSM Constants
 * @note Rewritten in typescript by mtyszczak
 * @author Stasel
 * @license MIT
 */

export const TIMEOUT_DEFAULT = 20000;
export const TIMEOUT_LONG    = TIMEOUT_DEFAULT * 4;

export const DEFAULT_BAUD_RATE = 115200;

export const CommandPrefix = "AT";
export type CommandPrefixType = typeof CommandPrefix;

export type GSMEventName = "messageReceive";

export enum ReturnCode {
  ok         = "OK",
  connect    = "CONNECT",
  ring       = "RING",
  noCarrier  = "NO CARRIER",
  error      = "ERROR",
  noDialtone = "NO DIALTONE",
  busy       = "BUSY",
  noAnswer   = "NO ANSWER"
};

export enum SignalStrengthDescription {
  /**
   * Excellent signal strength
   * @note applies only for signals -50 dBm and greater
   */
  excellent = "Excellent",
  /**
   * Good, reliable signal strength
   * @note applies only for signals in range -51 dBm to -60 dBm
   */
  good      = "Good",
  /**
   * The minimum for any service depending on a reliable connection and signal
   * strength, such as voice over Wi-Fi and non-HD video streaming
   * @note applies only for signals in range -61 dBm to -67 dBm
   */
  fair      = "Fair",
  /**
   * Should be enough for connecting to the network and light browsing and email
   * @note applies only for signals in range -68 dBm to -80 dBm
   */
  poor      = "Poor",
  /**
   * No signal or unsufficient signal to perform any action
   * @note applies only for signals less than -80 dBm
   */
  noSignal  = "No signal",
  /**
   * Signal could not be detected (board may not be compatible)
   */
  notKnown  = "Not known or not detectable"
};

export enum BerDescription {
  "less than 0.2%"  = 0,
  "0.2% to 0.4%"    = 1,
  "0.4% to 0.8%"    = 2,
  "0.8% to 1.6%"    = 3,
  "1.6% to 3.2%"    = 4,
  "3.2% to 6.4%"    = 5,
  "6.4% to 12.8%"   = 6,
  "more than 12.8%" = 7,
  "N/A"             = 99
};

export enum ServiceClass {
  data  = 0,
  fax   = 1,
  voice = 8
};

export enum CharacterSet {
  IRA  = "IRA",
  GSM  = "GSM",
  UCS2 = "UCS2"
}

export enum PhoneBookStorage {
  sim              = "SM",
  fixedDialing     = "FD",
  dialedCalls      = "DC",
  missedCalls      = "MC",
  receivedCalls    = "RC",
  ownNumber        = "ON",
  mobileEquipment  = "ME",
  emergencyNumbers = "EN",
  lastDialed       = "LD"
}

export enum PhoneNumberType {
  /**
   * National numbering scheme
   */
  national      = 129,
  /**
   * International numbering scheme (contains the character "+")
   */
  international = 145,
  /**
   * Text based ex: 'Vodafone'
   */
  text          = 208,
  /**
   * Text based ex: 'Vodafone'
   */
  text2         = 209
}

export enum MessageStorage {
  internal     = "ME",
  sim          = "SM",
  statusReport = "SR",
  all          = "MT"
}

export enum MessageFormat {
  PDU  = 0,
  text = 1
}

export enum MessageFilterText {
  "REC UNREAD" = 0,
  "REC READ"   = 1,
  "STO UNSENT" = 2,
  "STO SENT"   = 3,
  "ALL"        = 4
}

export enum MessageFilterPDU {
  unread       = 0,
  read         = 1,
  storedUnsent = 2,
  storedSent   = 3,
  all          = 4,
}

export enum MessageDeleteFilter {
  /**
   * Delete all read messages from storage, leaving unread messages and stored
   * mobile originated messages (whether sent or not) untouched
   */
  read              = 1,

  /**
   * Delete all read messages from storage and sent mobile originated messages,
   * leaving unread messages and unsent mobile originated messages untouched
   */
  readAndSent       = 2,

  /**
   *  Delete all read messages from storage, sent and unsent mobile originated
   *  messages, leaving unread messages untouched
   */
  readSentAndUnsent = 3,

  /**
   * Delete all message from storage
   */
  all               = 4
}

/* eslint-disable prefer-destructuring */
/* eslint-disable new-cap */
import { Commands, CommandRequest, make, makeWrite, makeRead, UnsolicitedResultCodes } from './commands';
import { DEFAULT_BAUD_RATE, CharacterSet, PhoneBookStorage,
  PhoneNumberType, MessageStorage, MessageFormat, MessageFilterPDU, MessageFilterText,
  MessageDeleteFilter, ReturnCode, GSMEventName } from './constants';
import { ConnectionError, TimeoutError, ParseError, UnsupportedEventName } from './errors';
import SerialPort from 'serialport';
import Parser, { Contact, Message, SignalQuality } from './parsing';

const GSM_PROMPT = ">",
  CTRL_Z = "\x1A";

export default class GSM {

  private serialPort:SerialPort;

  /**
   * @param {string} path path to the GSM Modem device (e.g.: '/dev/gsmmodem')
   * @param {number} baudRate device serial port baud rate (defaults to {@link DEFAULT_BAUD_RATE})
   * @public
   * @constructor
   */
  public constructor(
    path:string, baudRate:number = DEFAULT_BAUD_RATE
  ) {
    this.serialPort = new SerialPort( path, { baudRate, autoOpen: false } );
  }

  public on( event:GSMEventName, callback:(data:string)=>void ) {
    switch(event) {
    case "messageReceive":
      this.serialPort.addListener( "data", (data:Buffer) => {
        const str = data.toString( "utf8" ).trim(),
          command = `${UnsolicitedResultCodes.messageReceived}: `;
        if( str.startsWith( command ) )
          callback( str.substring( command.length ) );
      } );
      break;
    default:
      throw new UnsupportedEventName( `Given event name is not currently supported: '${event}'` );
    }
  }

  /**
   * Connects to the GSM modem via serial port
   * @throws {ConnectionError} on serial port connection error
   * @public
   * @async
   */
  public async connect():Promise<void> {
    await new Promise( (res:(__discard:unknown)=>void) => {
      this.serialPort.open( (error:Error|null|undefined) => {
        if( error )
          throw new ConnectionError( `Error connecting to serial port: '${error.message}'` );
        res(undefined);
      });
    });
  }

  /**
   * Disconnects from the GSM modem serial port
   * @public
   */
  public disconnect():void {
    this.serialPort.close();
  }

  /**
   * Returns no errors if the modem is operational
   * @returns {Promise<void>}
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async check():Promise<boolean> {
    return (await this.runCommand( make( Commands.check ) )).length === 0;
  }

  /**
   * Returns the manufacturer identification
   * @returns {Promise<string>} manufacturer info
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async getManufacturerInformation():Promise<string> {
    return (await this.runCommand( make( Commands.checkManufacturerInformation ) ))[0];
  }

  /**
   * Returns the model identification
   * @returns {Promise<string>} model id
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async getModelIdentification():Promise<string> {
    return (await this.runCommand( make( Commands.checkModelIdentification ) ))[0];
  }

  /**
   * Returns the software revision identification
   * @returns {Promise<string>} rev id
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async getRevisionIdentification():Promise<string> {
    return (await this.runCommand( make( Commands.checkRevisionIdentification ) ))[0];
  }

  /**
   * Returns the device board serial number
   * @returns {Promise<string>} serial number
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async getSerialNumber():Promise<string> {
    return (await this.runCommand( make( Commands.checkSerialNumber ) ))[0];
  }

  /**
   * Returns the value of the Internal Mobile Subscriber Identity stored in the SIM without command echo.
   * @returns {Promise<string>} subscriber id
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async getSubscriberId():Promise<string> {
    return (await this.runCommand( make( Commands.checkSubscriberId ) ))[0];
  }

  /**
   * Returns information about the device
   * @returns {Promise<string>} id info
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async getIdentificationInformation():Promise<string> {
    return (await this.runCommand( make( Commands.identificationInformation ) ))[0];
  }

  /**
   * Starts a call to the phone number given as parameter
   * @param {string} number Phone number to be dialed
   * @note the numbers accepted are 0-9 and *,#,”A”, ”B”, ”C”, ”D”,”+”
   * @note type of call (data, fax or voice) depends on last Active Service Class (+FCLASS) setting
   * @returns {Promise<void>}
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async dial( who:string ):Promise<void> {
    await this.runCommand( make( Commands.dial, { suffix: ";", commandArgsDelim: who } ) );
  }

  /**
   * Answer an incoming call if automatic answer is disabled
   * @returns {Promise<void>}
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async answer():Promise< void > {
    await this.runCommand( make( Commands.answer ) );
  }

  /**
   * Get the number of rings required before device automatically answers an incoming call.
   *  0 means auto answer is disabled
   * @returns {Promise<number>} number of rings
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async getAutoAnswerRings():Promise<number> {
    return parseInt( (await this.runCommand( makeRead( Commands.autoAnswerRings ) ))[0] );
  }

  /**
   * Sets the number of rings required before device automatically answers an incoming call
   * @param {number} numberOfRings number of rings before answer (between 0 to 255). Set to 0 to disable auto answer
   * @returns {Promise<void>}
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async setAutoAnswerRings( numberOfRings:number ):Promise<void> {
    return void (await this.runCommand( makeWrite( Commands.autoAnswerRings, numberOfRings ) ));
  }

  /**
   * Execution command is used to close the current conversation (voice, data or fax)
   * @note this command can be issued only in command mode
   * @returns {Promise<void>}
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async hangup():Promise<void> {
    return void (await this.runCommand( make( Commands.hangup ) ));
  }

  /**
   * Gets the current character set used by the device
   * @returns {Promise<CharacterSet>} character set
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async getCharacterSet():Promise<CharacterSet> {
    return CharacterSet[(await this.runCommand( makeRead( Commands.characterSet ) ))?.[0]?.replace(/^"?(.*?)"?$/, "$1") as keyof typeof CharacterSet];
  }

  /**
   * Sets the current character set used by the device
   * @param {CharacterSet} characterSet
   * @returns {Promise<void>}
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async setCharacterSet( characterSet:CharacterSet ):Promise<void> {
    return void (await this.runCommand( makeWrite( Commands.characterSet, characterSet ) ));
  }

  /**
   * Reports received signal quality
   * @returns {Promise<SignalQuality>}
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async getSignalQuality():Promise<SignalQuality> {
    return Parser.parseSignalQuality( (await this.runCommand( make( Commands.checkSignalQuality ) ))[0].split(",") );
  }

  /**
   * Returns the current GSM network operator selection
   * @returns {Promise<string|"Unknown">} operator
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async getCurrentOperator():Promise<string|"Unknown"> {
    const result = (await this.runCommand( makeRead( Commands.checkCurrentOperator ) ))?.[0]?.split(",");
    if( typeof result[2] !== "undefined" )
      return result[2].replace(/^"?(.*?)"?$/, "$1");

    return "Unknown";
  }

  /**
   * Selects phonebook memory storage
   * @param {PhoneBookStorage} storage Phone book storage type
   * @returns {Promise<void>}
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async setPhoneBookStorage( storage:PhoneBookStorage ):Promise<void> {
    return void (await this.runCommand( makeWrite( Commands.phoneBookStorage, storage ) ));
  }

  /**
   * Returns for a given phone book storage the maximum number of contacts and the used contact slots
   * @param {PhoneBookStorage} phoneBook Phone book storage type
   * @returns {Promise<{used:number;capacity:number;}>} phone book storage usage
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async getPhoneBookUsage( phoneBook:PhoneBookStorage ):Promise<{used:number;capacity:number;}> {
    await this.setPhoneBookStorage( phoneBook );
    const result = ( await this.runCommand( makeRead( Commands.phoneBookStorage ) ) )?.[0]?.split(",");

    return {
      used: parseInt(result?.[1]),
      capacity: parseInt(result?.[2])
    };
  }

  /**
   * Returns a range of contacts for a given phone book
   * @param {PhoneBookStorage} phoneBook Phone book storage type
   * @param {number} startIndex Lower edge of the contact index to fetch
   * @param {number} endIndex Upper edge of the contact index to fetch
   * @returns {Promise<Array<Contact>>} contacts
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async readPhoneBook( phoneBook:PhoneBookStorage, startIndex:number, endIndex:number ):Promise<Array<Contact>> {
    await this.setPhoneBookStorage( phoneBook );
    await this.setCharacterSet( CharacterSet.UCS2 );
    const result = await this.runCommand( makeWrite( Commands.phoneBookRead, startIndex, endIndex ) );

    return Parser.parseContacts( result );
  }

  /**
   * Adds a new contact to the end of a given phone book
   * @param {PhoneBookStorage} phoneBook Phone book storage type
   * @param {string} who The phone number of the contact
   * @param {PhoneNumberType} numberType Phone number type
   * @param {string} text Contact name
   * @returns {Promise<void>}
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async addContact( phoneBook:PhoneBookStorage, who:number, numberType:PhoneNumberType, text:string ):Promise<void> {
    await this.setPhoneBookStorage( phoneBook );
    await this.setCharacterSet( CharacterSet.UCS2 );
    await this.runCommand( makeWrite( Commands.phoneBookWrite, who, numberType, Parser.UCS2HexString( text ) ) );
  }

  /**
   * Removes a contact from a given phone book at a given index
   * @param {PhoneBookStorage} phoneBook Phone book storage type
   * @param {number} index The index of the contact to delete
   * @returns {Promise<void>}
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async deleteContact( phoneBook:PhoneBookStorage, index:number ):Promise<void> {
    await this.setPhoneBookStorage(phoneBook);
    await this.runCommand( makeWrite( Commands.phoneBookWrite, index ) );
  }

  /**
   * Updates or creates a contact from a given phone book at a given index
   * @param {PhoneBookStorage} phoneBook Phone book storage type
   * @param {number} index The index of the contact to update
   * @param {string} who The phone number of the contact
   * @param {PhoneNumberType} numberType Phone number type
   * @param {string} text Contact name
   * @returns {Promise<void>}
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async setContact( phoneBook:PhoneBookStorage, index:number, who:string, numberType:PhoneNumberType, text:string ):Promise<void> {
    await this.setPhoneBookStorage( phoneBook );
    await this.setCharacterSet( CharacterSet.UCS2 );
    await this.runCommand( makeWrite( Commands.phoneBookWrite, index, who, numberType, Parser.UCS2HexString( text ) ) );
  }

  /**
   * @param {MessageStorage} readStorage memory from which messages are read and deleted
   * @returns {Promise<void>}
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async setPreferredMessageStorage( readStorage:MessageStorage ):Promise<void> {
    return void (await this.runCommand( makeWrite( Commands.preferredMessageStorage, readStorage, MessageStorage.sim, MessageStorage.sim ) ));
  }

  /**
   * Returns the current format of messages used with send, list, read and write command
   * @returns {Promise<MessageFormat>}
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async getMessageFormat():Promise<MessageFormat> {
    return parseInt((await this.runCommand( makeRead( Commands.messageFormat ) ))[0]);
  }

  /**
   * Sets the format of messages used with send, list, read and write commands.
   * @param {MessageFormat} format The message format to use
   * @returns {Promise<void>}
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async setMessageFormat( format:MessageFormat ):Promise<void> {
    return void (await this.runCommand( makeWrite( Commands.messageFormat, format ) ));
  }

  /**
   * Returns a list of all SMS messages for a given storage and filter
   * @note changes the message format to text and character set to UCS2
   * @param {MessageStorage} storage The message storage to read from
   * @param {MessageFilter} filter A filter to select messages by status
   * @returns {Promise<Array<Message>>} messages
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async readSMS( storage:MessageStorage, filter:MessageFilterPDU ):Promise<Array<Message>> {
    await this.setMessageFormat( MessageFormat.text );
    await this.setPreferredMessageStorage( storage );
    await this.setCharacterSet( CharacterSet.UCS2 );
    await this.runCommand( makeWrite( Commands.textModeParameters, 1 ) );
    const result = await this.runCommand( makeWrite( Commands.listPreferredStoreMessages, MessageFilterText[filter] ) );

    return Parser.parseTextMessageResult( result );
  }

  /**
   * Sends a SMS message to the destination number
   * @param {string} msisdn Destination number
   * @param {string} message Text message to
   * @returns {string} Reference ID if the delivery was successful
   * @returns {Promise<void>} messages
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  public async sendSMS( msisdn:string, message:string ):Promise<void> {
    await this.setCharacterSet( CharacterSet.UCS2 );
    await this.setMessageFormat( MessageFormat.text );
    await this.runCommand( makeWrite( Commands.sendMessage, Parser.UCS2HexString( msisdn ) )); // Returns a prompt > for a message
    await this.runCommand( make( Parser.UCS2HexString( message ), { prefix: "", suffix: CTRL_Z } ) );
  }

  /**
   * Deletes a message from storage
   * @param {MessageStorage} storage The message storage to delete from
   * @param {number} index  The index of the message to delete
   * @returns {Promise<void>} messages
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  async deleteMessage( storage:MessageStorage, index:number ):Promise<void> {
    await this.setPreferredMessageStorage(storage);
    await this.runCommand( makeWrite( Commands.deleteMessage, index ) );
  }

  /**
   * Deletes multiple messages from storage according to the give filter
   * @param {MessageStorage} storage The message storage to delete from
   * @param {MessageDeleteFilter} filter The delete filter to use
   * @returns {Promise<void>} messages
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   * @public
   * @async
   */
  async deleteAllMessages( storage:MessageStorage, filter:MessageDeleteFilter ):Promise<void> {
    await this.setPreferredMessageStorage(storage);
    await this.runCommand( makeWrite( Commands.deleteMessage, 0, filter ) );
  }

  /**
   * Sends given command to the GSM modem and returns the processed output
   * @example `+CSQ` (command) -> `AT+CSQ\r\r\n+CSQ: 25,99\r\n\r\nOK\r\n` (unprocessed output) -> [ `25,99` ] (output)
   *
   * @param {CommandRequest} command command to be executed or custom command as string
   * @param {{timeout?:number;removePrefix?:boolean;prefixDelim?:string;}} settings command settings
   *
   * @returns {CommandResponseArguments} parsed command output
   *
   * @throws {ConnectionError} when device is not connected
   * @throws {TimeoutError} on timeout
   * @throws {ResponseError} on error detected in response
   *
   * @public
   * @async
   */
  // eslint-disable-next-line require-await
  public async runCommand( command:CommandRequest, settings:{timeout?:number;removePrefix?:boolean;prefixDelim?:string;} = {} ) {
    return new Promise( (resolve:(res:Array<string>)=>void) => {
      settings = { timeout: 0, removePrefix: true, prefixDelim: ": ", ...settings };

      if( !this.serialPort.isOpen )
        throw new ConnectionError( "Device is not Connected");

      // @ts-expect-error Timeout number
      let timeoutHandle:Timeout;
      if( settings.timeout )
        timeoutHandle = setTimeout(() => {
          throw new TimeoutError("Command timeout");
        }, settings.timeout);

      let output = "";

      const dataHandler = ( data:Buffer ) => {
        output += data.toString("utf8");
        const currentDataTrim = data.toString("utf8").trim();

        if(currentDataTrim.endsWith(ReturnCode.ok) || currentDataTrim.endsWith(GSM_PROMPT) || currentDataTrim.endsWith(ReturnCode.error)) {
          if( settings.timeout && typeof timeoutHandle !== "undefined" )
            clearTimeout( timeoutHandle );
          this.serialPort.removeListener("data", dataHandler);
        }

        // OK message - success
        if( currentDataTrim.endsWith(ReturnCode.ok) ) {
          setTimeout( () => {
            if( output.substring( 0, command.length ) === command )
              output = output.substring( command.length );

            const result = output.split("\r\n")
              .filter( val => val.length )
              .map( val => val.trim() )
              .slice( 0, -1 );
            if( settings.removePrefix && result.length )
              result[0] = result[0].split( settings.prefixDelim as string )[1];

            resolve(result);
          }, 20 );

          return undefined;
        } else if(currentDataTrim.endsWith(ReturnCode.error)) // ERROR message - failure
          throw new ParseError( `Encourted an error during the receive from the GDM modem: '${output}'` );
        // > message - prompt for user data
        else if(currentDataTrim.endsWith(GSM_PROMPT))
          setTimeout(() => {
            resolve( [ output ] );
          }, 20);
        // Else Partial message, wait for more data

        return undefined;
      };

      this.serialPort.addListener("data", dataHandler);
      this.serialPort.write(command);
      this.serialPort.flush();
    });
  }

  /**
   * @returns {string} string representation of the current instance
   */
  public toString():string {
    return `[object GSM(${this.serialPort.isOpen ? "Connected" : "Not Connected"})]`;
  }
}

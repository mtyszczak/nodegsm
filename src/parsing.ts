/**
 * @fileoverview GSM-related data parser
 * @note Rewritten in typescript by mtyszczak
 * @author Stasel
 * @license MIT
 */

import { CommandResponseArguments } from './commands';
import { SignalStrengthDescription, PhoneNumberType, BerDescription } from './constants';

export type SignalQuality = {
  description:SignalStrengthDescription;
  dBm:number;
  bitErrorRate:string;
};

export type Contact = {
  index: number;
  numberType: PhoneNumberType;
  number: string;
  text: string;
};

export type Message = {
  index: number;
  status: string;
  sender: string;
  time: Date,
  text: string,
  numberType: PhoneNumberType,
  rawHeader: string,
  rawMessage: string,
}

export default class Parser {

  /**
   * Parse signal from the type type <signalStrength,bitErrorRate>
   * @param {Array<string>} input signal input
   * @returns {SignalQuality} parsed signal quality
   * @public
   * @static
   */
  public static parseSignalQuality( input:CommandResponseArguments ):SignalQuality {
    const signalStrength = parseInt(input[0]),
      bitErrorRate = parseInt(input[1]);
    let dBm = NaN,
      description = SignalStrengthDescription.noSignal;
    if( signalStrength !== 99 ) {
      dBm = -113 + (signalStrength * 2);
      description = Parser.dbmDescription( dBm );
    }

    return {
      description,
      dBm,
      bitErrorRate: BerDescription[bitErrorRate] || "N/A"
    };
  }

  private static dbmDescription( dbm:number ):SignalStrengthDescription {
    if(dbm >= -50)
      return SignalStrengthDescription.excellent;
    if(dbm >= -60)
      return SignalStrengthDescription.good;
    if(dbm >= -67)
      return SignalStrengthDescription.fair;
    if(dbm >= -80)
      return SignalStrengthDescription.poor;
    if(dbm >= -110)
      return SignalStrengthDescription.noSignal;

    return SignalStrengthDescription.notKnown;
  }

  /**
   * Parses contacts from the {@link GSM#runCommand} `AT+CMGL` command
   * @param {CommandResponseArguments} input contacts
   * @returns {Array<Message>} parsed contacts
   * @example raw input:
   * +CMGL: 0,"REC UNREAD","002B003100310031003200320032003300330033003400340034",,"19/07/07,20:40:54+08",145,15
   * 00480065006C006C006F00200057006F0072006C006400210020D83CDF0D
   * @public
   * @static
   */
  public static parseTextMessageResult( list:CommandResponseArguments ):Array<Message> {
    const messages = [];
    for(let i = 0; i < list.length; i += 2) {
      const parts = list[i].split(","),

        index = parseInt(parts[0]),
        status = parts[1].replace(/^"?(.*?)"?$/, "$1"),
        sender = Parser.decodeUCS2Hex( parts[2].replace(/^"?(.*?)"?$/, "$1") ),
        date = parts[4].replace(/^"?(.*?)"?$/, "$1").replace(/\//g, "-"),
        time = parts[5].replace(/^"?(.*?)"?$/, "$1"),
        numberType = parseInt(parts[6]),
        textLength = parseInt(parts[7]),
        messageText = list[i + 1],

        // Message Text decode. Can be either UTF-8 or UCS2. We check this by the char byte size
        textBuffer = Buffer.from(messageText, "hex"),
        textCharSize = textBuffer.length / textLength,
        decodedText = textCharSize === 2 ? Parser.decodeUCS2Hex( messageText ) : textBuffer.toString("utf8"),

        // Time calculate
        timeParts = time.split("+"),
        timezone = parseFloat(timeParts[1]) / 4.0 * 100,
        timezonePrefix =  timezone > 0 ? "+" : "-",
        timezoneString = timezonePrefix + Math.abs(timezone).toString()
          .padStart(4, "0"),
        timeStamp = `20${date}T${timeParts[0]}${timezoneString}`,
        senderString =  [ PhoneNumberType.text, PhoneNumberType.text2 ].includes(numberType) ? Parser.decodeFromAscii( sender ) : sender;
      messages.push({
        index,
        status,
        sender: senderString,
        time: new Date(timeStamp),
        text: decodedText,
        numberType,
        rawHeader: list[i],
        rawMessage: messageText
      });
    }

    return messages;
  }


  /**
   * Parses contacts from the {@link GSM#runCommand} `AT+CPBR` command
   * @param {CommandResponseArguments} input contacts
   * @returns {Array<Contact>} parsed contacts
   * @example raw input:
   * +CPBR: 1,"+31654501233",145,"0056006F006900630065006D00610069006C002000620065006C006C0065006E"
   * +CPBR: 2,"1800",129,"00310038003000300020006E0075006D006D006500720069006E0066006F"
   * +CPBR: 3,"+31654500100",145,"004B006C0061006E00740065006E0073006500720076006900630065"
   * @public
   * @static
   */
  public static parseContacts( input:CommandResponseArguments ):Array<Contact> {
    return input.map( record => {
      const parts = record.split(",");

      return {
        index: parseInt(parts[0]),
        numberType: parseInt(parts[2]),
        number: parts[1].replace(/^"?(.*?)"?$/, "$1"),
        text: Parser.decodeUCS2Hex( parts[3].replace(/^"?(.*?)"?$/, "$1") )
      };
    });
  }

  public static decodeUCS2Hex( data:string ) {
    return Buffer.from(data, "hex").swap16()
      .toString("ucs2");
  }

  public static UCS2HexString( data:string ) {
    return Buffer.from(data, "ucs2").swap16()
      .toString("hex")
      .toUpperCase();
  }

  private static decodeFromAscii( data:string ):string {
    let output = "",
      i = 0;
    while( i < data.length )
      if( parseInt(data[i]) < 2 && i + 2 < data.length ) {
        output += String.fromCharCode(parseInt(data[i] + data[i + 1] + data[i + 2]));
        i += 3;
      } else if( i + 1 < data.length ) {
        output += String.fromCharCode(parseInt(data[i] + data[i + 1]));
        i += 2;
      } else
        i += 1;

    return output;
  }

}
